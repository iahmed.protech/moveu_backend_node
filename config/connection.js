const development = {
  database: 'moveu_prova',
  username: 'root',
  password: 'admin!@#123ADMIN',
  host: 'localhost',
  dialect: 'mysql',
};

const testing = {
  database: 'databasename',
  username: 'username',
  password: 'password',
  host: 'localhost',
  dialect: 'mysql',
};

const production = {
  database: 'demo_moveu',
  username: 'demo_moveu',
  password: 'Quv7v13&',
  host: 'localhost',
  dialect: 'mysql',
};

module.exports = {
  development,
  testing,
  production,
};
