const privateRoutes = {
  'GET /users': 'UserController.getAll',
  'GET /getposts/:post_type/:vehicle_id?/:page_number?': 'PostController.getPosts',
  'GET /getDrivers/:booking_id': 'PostController.getDriverList',
  'GET /getpost/:post_type/:post_id': 'PostController.getPost',
  'POST /selectdriver': 'PostController.setDriverForBooking',
  'GET /getDriverForBooking/:booking_id': 'PostController.getDriverForBooking',
  'POST /saveBusinessHours': 'PostController.saveBusinessHours',
  'POST /saveUnavailableTime': 'PostController.saveUnavailableTime',
  'POST /deleteUnavailableTime': 'PostController.deleteUnavailableTime',
  'POST /changeVehicleStatus': 'PostController.changeVehicleAvailabilityStatus',
  'POST /saveDriverInfo': 'PostController.saveDriverInfo',
  'GET /getCarsBookingCount': 'PostController.getCarsBookingCount',
  'GET /getBillingDetail': 'PostController.getBillingDetail',
  'GET /getBookingsWithBilling/:page_number/:booking_type': 'PostController.getBookingsWithBilling',
  'GET /getBookings/:post_type/:vehicle_id?/:page_number?/:upcoming_bookings?': 'PostController.getBookings',
  'PUT /setBookingStatus': 'PostController.setBookingToComplete'
};

module.exports = privateRoutes;
