DELIMITER $
$
CREATE PROCEDURE `GetBookings`
(IN postId INT, IN bookingId INT, IN vehicleId INT, IN pageNumber INT, IN upcomingBookings INT)
BEGIN
    Declare startIndex INT;
Set startIndex
= pageNumber * 5;
Select p.ID,
    pbs.meta_value as 'booking_status',
    pput.meta_value as 'pickup_time',
    ppud.meta_value as 'pickup_date',
    ppfd.meta_value as 'full_date',
    pvehicle.meta_value as 'vehicle_name',
    pvehicleid.meta_value as 'vehicle_id',
    pclntfname.meta_value as 'client_first_name',
    pclntlname.meta_value as 'client_last_name',
    pclntphone.meta_value as 'client_phone_number',
    Replace(pcoordinates.meta_value,'"','^') as 'coordinates',
    Replace(pextra.meta_value, '"', '^') as 'extra',
    pcmnt.meta_value as 'notes',
    pdriverId.meta_value as 'driver_id',
    pdriverfname.meta_value as 'driver_first_name',
    pdriverlname.meta_value as 'driver_last_name',
    pflightid.meta_value as 'flight_id',
    pnopassenger.meta_value as 'num_of_passenger'
from ccB4bP116_posts p
    left join ccB4bP116_postmeta pbs on pbs.post_id = p.ID and pbs.meta_key = 'chbs_booking_status_id'
    left join ccB4bP116_postmeta pput on pput.post_id = p.ID and pput.meta_key = 'chbs_pickup_time'
    left join ccB4bP116_postmeta ppud on ppud.post_id = p.ID and ppud.meta_key = 'chbs_pickup_date'
    left join ccB4bP116_postmeta ppfd on ppfd.post_id = p.ID and ppfd.meta_key = 'chbs_pickup_datetime'
    left join ccB4bP116_postmeta pvehicle on pvehicle.post_id = p.ID and pvehicle.meta_key = 'chbs_vehicle_name'
    left join ccB4bP116_postmeta pvehicleid on pvehicleid.post_id = p.ID and pvehicleid.meta_key = 'chbs_vehicle_id'
    left join ccB4bP116_postmeta pclntfname on pclntfname.post_id = p.ID and pclntfname.meta_key = 'chbs_client_contact_detail_first_name'
    left join ccB4bP116_postmeta pclntlname on pclntlname.post_id = p.ID and pclntlname.meta_key = 'chbs_client_contact_detail_last_name'
    left join ccB4bP116_postmeta pclntphone on pclntphone.post_id = p.ID and pclntphone.meta_key = 'chbs_client_contact_detail_phone_number'
    left join ccB4bP116_postmeta pcoordinates on pcoordinates.post_id = p.ID and pcoordinates.meta_key = 'chbs_coordinate'
    left join ccB4bP116_postmeta pextra on pextra.post_id = p.ID and pextra.meta_key = 'chbs_booking_extra'
    left join ccB4bP116_postmeta pcmnt on pcmnt.post_id = p.ID and pcmnt.meta_key = 'chbs_comment'
    left join ccB4bP116_postmeta pdriverId on pdriverId.post_id = p.ID and pdriverId.meta_key = 'chbs_driver_id'
    left join ccB4bP116_postmeta pdriverfname on pdriverfname.post_id = pdriverId.meta_value and pdriverfname.meta_key = 'chbs_first_name'
    left join ccB4bP116_postmeta pdriverlname on pdriverlname.post_id = pdriverId.meta_value and pdriverlname.meta_key = 'chbs_second_name'
    left join ccB4bP116_postmeta pflightid on pflightid.post_id = p.ID and pflightid.meta_key = 'chbs_form_element_field'
    left join ccB4bP116_postmeta pnopassenger on pnopassenger.post_id = p.ID and pnopassenger.meta_key = 'chbs_passenger_count'
where ((p.Id in (																																																																																																																																																																																																																																																																																																																														
SELECT post_id
    FROM
        ccB4bP116_postmeta
    Where meta_value = postId) and bookingId = -1) OR p.Id = bookingId)
    and (pvehicleid.meta_value = vehicleId OR vehicleId = -1)
    and p.post_type = 'chbs_booking'
    and (pbs.meta_value = 2 or pbs.meta_value = 4)
    and p.post_status = 'publish'
    and
    ((ppfd.meta_value >= now() AND upcomingBookings = 1 OR bookingId <> -1)
    OR (ppfd.meta_value < now() AND upcomingBookings = 0 OR bookingId <> -1)
    )

Order by full_date asc
-- Order by pickup_date asc, pickup_time asc
        limit startIndex,5;
END








DELIMITER
$$
CREATE PROCEDURE `GetPosts`
(IN postId INT, IN columnsArray varchar
(8000))
BEGIN
    Select columnsArray;
            Select meta_key, meta_value
        from ccB4bP116_postmeta
        where post_id = postId
        group by meta_key, meta_value
        having find_in_set(meta_key, columnsArray)
        -- having meta_key in('chbs_passenger_count','chbs_bag_count','chbs_plate_number')
    UNION
        Select 'vehicle_title' as meta_key, post_title as meta_value
        from ccB4bP116_posts
        where Id = postId;

    -- Drop procedure GetVisitStatsByMonths
    END$$
DELIMITER
;

DELIMITER $$
CREATE PROCEDURE `GetBookingsForBilling`
(IN postId INT, IN pageNumber INT, IN bookingType INT)
BEGIN
    Declare startIndex INT;
Set startIndex
= pageNumber * 5;
Select p.ID,
    pbs.meta_value as 'booking_status',
    pput.meta_value as 'pickup_time',
    ppud.meta_value as 'pickup_date',
    pvehicle.meta_value as 'vehicle_name',
    pvehicleid.meta_value as 'vehicle_id',
    pclntfname.meta_value as 'client_first_name',
    pclntlname.meta_value as 'client_last_name',
    pclntphone.meta_value as 'client_phone_number',
    Replace(pcoordinates.meta_value,'"','^') as 'coordinates',
    Replace(pextra.meta_value, '"', '^') as 'extra'
from ccB4bP116_posts p
    left join ccB4bP116_postmeta pbs on pbs.post_id = p.ID and pbs.meta_key = 'chbs_booking_status_id'
    left join ccB4bP116_postmeta pput on pput.post_id = p.ID and pput.meta_key = 'chbs_pickup_time'
    left join ccB4bP116_postmeta ppud on ppud.post_id = p.ID and ppud.meta_key = 'chbs_pickup_date'
    left join ccB4bP116_postmeta pvehicle on pvehicle.post_id = p.ID and pvehicle.meta_key = 'chbs_vehicle_name'
    left join ccB4bP116_postmeta pvehicleid on pvehicleid.post_id = p.ID and pvehicleid.meta_key = 'chbs_vehicle_id'
    left join ccB4bP116_postmeta pclntfname on pclntfname.post_id = p.ID and pclntfname.meta_key = 'chbs_client_contact_detail_first_name'
    left join ccB4bP116_postmeta pclntlname on pclntlname.post_id = p.ID and pclntlname.meta_key = 'chbs_client_contact_detail_last_name'
    left join ccB4bP116_postmeta pclntphone on pclntphone.post_id = p.ID and pclntphone.meta_key = 'chbs_client_contact_detail_phone_number'
    left join ccB4bP116_postmeta pcoordinates on pcoordinates.post_id = p.ID and pcoordinates.meta_key = 'chbs_coordinate'
    left join ccB4bP116_postmeta pextra on pextra.post_id = p.ID and pextra.meta_key = 'chbs_booking_extra'
where ((p.Id in (																																																																																																																																																																																																																																																																																																																														
SELECT post_id
    FROM
        ccB4bP116_postmeta
    Where meta_value = postId)))
    and p.post_type = 'chbs_booking'
    and p.post_status = 'publish'
    and (
        (bookingType = 0 and (pbs.meta_value = 2 or pbs.meta_value = 4))
    or (bookingType = 1 and (pbs.meta_value = 4 or pbs.meta_value = 5 or pbs.meta_value = 6)))
Order by pickup_date desc, pickup_time asc
        limit startIndex,5;
END$$
DELIMITER ;

