const sequelize = require('../../config/database');
const { QueryTypes } = require('sequelize');
const moment = require('moment');
const PHPUnserialize = require('php-unserialize');
const axios = require('axios');
const unserialize = require('locutus/php/var/unserialize');

const PostController = () => {
    const getPosts = async (req, res) => {
        // const { body } = req;
        const post_type = req.params.post_type;
        const post_id = req.post_id;
        let vehicle_id = -1;
        let page_number = 0;
        if (req.params.vehicle_id != undefined) {
            vehicle_id = req.params.vehicle_id;
        }
        if (req.params.page_number != undefined) {
            page_number = req.params.page_number;
        }
        let booking_id = -1;
        if (req.params.booking_id != undefined) {
            booking_id = req.params.booking_id;
        }
        console.log(req.params);
        console.log(post_id);
        console.log(post_type);
        console.log(booking_id);
        if (post_id != null || post_type != null) {
            try {
                let query = '';
                let posts;
                if (post_type == 'chbs_vehicle') { // get vehicles list
                    query = `Select p.*, (Select guid from ccB4bP116_posts 
                    where ID IN (Select meta_value from ccB4bP116_postmeta where post_id = p.ID and meta_key = '_thumbnail_id')) as imgUrl
                    from ccB4bP116_posts p
                    where p.Id 
                    in (SELECT post_id FROM 
                    ccB4bP116_postmeta Where meta_value = ?) and p.post_type = 'chbs_vehicle'
                    and p.post_status = 'publish'
                    `;
                    posts = await sequelize.query(query, { replacements: [post_id, post_type], type: QueryTypes.SELECT });
                }
                else if (post_type == 'chbs_booking') { // get booking list
                    posts = await sequelize.query('CALL GetBookings(?,?,?,?, -1)', { replacements: [post_id, -1, vehicle_id, page_number] });
                } else if (post_type == 'driver_options') {
                    query = `
                    SELECT post_id, meta_key, meta_value FROM 
                    ccB4bP116_postmeta where 
                    post_id = ?
                    and meta_key in 
                    ('chbs_vehicle_company_id','chbs_first_name', 
                    'chbs_second_name', 'chbs_driving_license', 
                    'chbs_contact_email_address', 'chbs_contact_phone_number')
                    UNION
                    Select ? as post_id, 'chbs_vehicle_company_name' as meta_key, post_title as meta_value
                    from ccB4bP116_posts where ID = (Select meta_value from ccB4bP116_postmeta where post_id = ? and meta_key = 'chbs_vehicle_company_id')
                    and post_status = 'publish'
                    `;
                    posts = await sequelize.query(query, { replacements: [post_id, post_id, post_id], type: QueryTypes.SELECT });
                } else if (post_type == 'nextBooking') {
                    query = `
                    Select p.ID, 
                    pbs.meta_value as 'booking_status',
                    pput.meta_value as 'pickup_time',
                    ppud.meta_value as 'pickup_date',
                    pvehicle.meta_value as 'vehicle_name',
                    pclntfname.meta_value as 'client_first_name',
                    pclntlname.meta_value as 'client_last_name',
                    pickupdatetime.meta_value as 'pickup_date_time',
                    Replace(pcoordinates.meta_value,'"','^') as 'coordinates'
                    from ccB4bP116_posts p 
                    left join ccB4bP116_postmeta pbs on pbs.post_id = p.ID and pbs.meta_key = 'chbs_booking_status_id'
                    left join ccB4bP116_postmeta pput on pput.post_id = p.ID and pput.meta_key = 'chbs_pickup_time'
                    left join ccB4bP116_postmeta ppud on ppud.post_id = p.ID and ppud.meta_key = 'chbs_pickup_date'
                    left join ccB4bP116_postmeta pvehicle on pvehicle.post_id = p.ID and pvehicle.meta_key = 'chbs_vehicle_name'
                    left join ccB4bP116_postmeta pclntfname on pclntfname.post_id = p.ID and pclntfname.meta_key = 'chbs_client_contact_detail_first_name'
                    left join ccB4bP116_postmeta pclntlname on pclntlname.post_id = p.ID and pclntlname.meta_key = 'chbs_client_contact_detail_last_name'
                    left join ccB4bP116_postmeta pcoordinates on pcoordinates.post_id = p.ID and pcoordinates.meta_key = 'chbs_coordinate'
                    left join ccB4bP116_postmeta pickupdatetime on pickupdatetime.post_id = p.ID and pickupdatetime.meta_key = 'chbs_pickup_datetime'
                    where ((p.Id in (																																																																																																																																																																																																																																																																																																																														
                    SELECT post_id FROM																										
                    ccB4bP116_postmeta 
                    Where meta_value = ?)))
                    and pbs.meta_value = 2
                    and pickupdatetime.meta_value >= curdate()
                    and p.post_type = 'chbs_booking'
                    and p.post_status = 'publish'
                    Order by pickup_date asc, pickup_time asc
                    limit 1;`;
                    posts = await sequelize.query(query, { replacements: [post_id], type: QueryTypes.SELECT });
                }

                return res.status(200).json({ posts });
            } catch (err) {
                console.trace(err);
                return res.status(500).json({ err });
            }
        }
        return res.status(400).json({ msg: 'Bad Request: Some Parameters missing!' });
    };

    const getBookings = async (req, res) => {
        const post_id = req.post_id;
        let vehicle_id = -1;
        let page_number = 0;
        let upcoming_bookings = true;
        if (req.params.vehicle_id != undefined) {
            vehicle_id = req.params.vehicle_id;
        }
        if (req.params.page_number != undefined) {
            page_number = req.params.page_number;
        }
        let booking_id = -1;
        if (req.params.booking_id != undefined) {
            booking_id = req.params.booking_id;
        }
        if (req.params.upcoming_bookings != undefined) {
            upcoming_bookings = req.params.upcoming_bookings;
        }

        if (post_id != null || post_type != null) {
            try {
                let query = '';
                let posts;
                posts = await sequelize.query('CALL GetBookings(?,?,?,?,?)', { replacements: [post_id, -1, vehicle_id, page_number, upcoming_bookings] });
                return res.status(200).json({ posts });
            } catch (err) {
                console.trace(err);
                return res.status(500).json({ err });
            }
        }
        return res.status(400).json({ msg: 'Bad Request: Some Parameters missing!' });
    };

    const getDriverList = async (req, res) => {
        try {
            const post_id = req.post_id;
            let booking_id = req.params.booking_id;
            let query = ``;
            // total drivers of this company
            query = `Select p.ID, pmfname.meta_value as first_name, pmlname.meta_value as last_name,
            Replace(pmexcludedate.meta_value,'"','^') as exclude_dates
            from ccB4bP116_posts p 
            left join ccB4bP116_postmeta pmfname on pmfname.post_id = p.ID and pmfname.meta_key = 'chbs_first_name'
            left join ccB4bP116_postmeta pmlname on pmlname.post_id = p.ID and pmlname.meta_key = 'chbs_second_name'
            left join ccB4bP116_postmeta pmexcludedate on pmexcludedate.post_id = p.ID and pmexcludedate.meta_key = 'chbs_date_exclude'
            where p.Id in (SELECT post_id FROM ccB4bP116_postmeta Where meta_value = ?)
            and p.post_status = 'publish'
            and p.post_type	= 'chbs_driver'`;
            all_drivers = await sequelize.query(query, { replacements: [post_id], type: QueryTypes.SELECT });

            // drivers which are booked
            query = `Select p.ID,
            pbs.meta_value as 'booking_status',
            ppudt.meta_value as 'pickup_date_time',
            driver.meta_value as 'driver_id',
            date_add(ppudt.meta_value, interval duration.meta_value minute) as 'drop_date_time' 
            from ccB4bP116_posts p
            left join ccB4bP116_postmeta pbs on pbs.post_id = p.ID and pbs.meta_key = 'chbs_booking_status_id'
            left join ccB4bP116_postmeta ppudt on ppudt.post_id = p.ID and ppudt.meta_key = 'chbs_pickup_datetime'
            left join ccB4bP116_postmeta duration on duration.post_id = p.ID and duration.meta_key = 'chbs_duration'
            left join ccB4bP116_postmeta driver on driver.post_id = p.ID and driver.meta_key = 'chbs_driver_id'
            where ((p.Id in (																																																																																																																																																																																																																																																																																																																														
            SELECT post_id
            FROM ccB4bP116_postmeta
            Where meta_value = ?)))
            and p.ID <> ?
            and driver.meta_value <> -1
            and p.post_type = 'chbs_booking'
            and p.post_status = 'publish'
            and (pbs.meta_value = 2 or pbs.meta_value = 4)`;
            allBookings = await sequelize.query(query, { replacements: [post_id, booking_id], type: QueryTypes.SELECT });
            console.log('booked drivers')
            console.log(allBookings);

            // current booking pickup time and dropoff time 
            query = `Select * from 
            (Select ppudt.meta_value as 'pickup_date_time',
            date_add(ppudt.meta_value, interval duration.meta_value minute) as 'drop_date_time'
            from ccB4bP116_posts p
            left join ccB4bP116_postmeta pbs on pbs.post_id = p.ID and pbs.meta_key = 'chbs_booking_status_id'
            left join ccB4bP116_postmeta ppudt on ppudt.post_id = p.ID and ppudt.meta_key = 'chbs_pickup_datetime'
            left join ccB4bP116_postmeta duration on duration.post_id = p.ID and duration.meta_key = 'chbs_duration'
            where ((p.Id in (																																																																																																																																																																																																																																																																																																																														
            SELECT post_id
            FROM
            ccB4bP116_postmeta
            Where meta_value = ?)))
            and p.ID = ?
            and p.post_type = 'chbs_booking'
            and p.post_status = 'publish'
            and (pbs.meta_value = 2 or pbs.meta_value = 4)) as bookingTiming`;
            currBookingTimings = await sequelize.query(query, { replacements: [post_id, booking_id], type: QueryTypes.SELECT });
            console.log(currBookingTimings);
            let booking_pickup_time = currBookingTimings[0].pickup_date_time;
            let drop_date_time = currBookingTimings[0].drop_date_time;

            let conflicting_drivers_ids = [];
            allBookings.forEach(x => {
                if (moment(booking_pickup_time).isSameOrAfter(moment(x.pickup_date_time)) && moment(booking_pickup_time).isSameOrBefore(moment(x.drop_date_time))) {
                    conflicting_drivers_ids.push(parseInt(x.driver_id));
                }
            })
            console.log(conflicting_drivers_ids);
            console.log(all_drivers);

            // drivers not busy in other bookings
            let posts = all_drivers.filter(x => {
                return !conflicting_drivers_ids.includes(x.ID);
            });

            console.log(posts);
            posts.forEach(x => {
                // isDriverNotAvailable(PHPUnserialize.unserialize(x.exclude_dates));
                let raw_arr = x.exclude_dates.split('^');
                // console.log(raw_arr)
                if (isDriverBusy(raw_arr, booking_pickup_time)) {
                    x.is_busy = true;
                };
                // console.log(extractData(raw_arr));
                // let not_available_arr = extractData(raw_arr);
                // console.log(not_available_arr);
            });
            return res.status(200).json({ posts });
        } catch (err) {
            return res.status(500).json({ err });
        }


    }
    function isDriverNotAvailable(unavailableTimeArr) {
        // unavailableTimeArr.forEach(x => {
        //     console.log('inside func');
        //     console.log(x);
        // })
        console.log(unavailableTimeArr)
    }
    // get all indexes of given value in array
    function getAllIndexes(arr, val) {
        let indexes = [], i = -1;
        while ((i = arr.indexOf(val, i + 1)) != -1) {
            indexes.push(i);
        }
        return indexes;
    }
    // this function extracts data from raw_arr.
    function isDriverBusy(arr, booking_pickup_time) {
        let isBusy = false;
        let start_date_indexes = getAllIndexes(arr, 'startDate');
        let stop_date_indexes = getAllIndexes(arr, 'stopDate');
        let start_time_indexes = getAllIndexes(arr, 'startTime');
        let stop_time_indexes = getAllIndexes(arr, 'stopTime');
        // let unavailableTimeArr = [];
        let arr_length = start_date_indexes.length;
        for (let i = 0; i < arr_length; i++) {
            let unavailableTime = new Object();
            let start_date = arr[start_date_indexes[i] + 2];
            let start_time = arr[start_time_indexes[i] + 2];
            let stop_date = arr[stop_date_indexes[i] + 2];
            let stop_time = arr[stop_time_indexes[i] + 2];

            console.log('stop');
            let strtDateArr = start_date.split('-');
            console.log(strtDateArr);
            let stopDateArr = stop_date.split('-');
            console.log(stopDateArr);
            // let d = new Date(strtDateArr[2], strtDateArr[1] - 1, strtDateArr[0]);
            // console.log('and ddate')
            // console.log(d);  
            let strtString = strtDateArr[1] + "-" + strtDateArr[0] + "-" + strtDateArr[2] + " " + start_time;
            let stopString = stopDateArr[1] + "-" + stopDateArr[0] + "-" + stopDateArr[2] + " " + stop_time;
            console.log('----------------');
            console.log(moment(strtString).format('MM-DD-YYYY hh:mm'))
            console.log(moment(stopString).format('MM-DD-YYYY hh:mm'))


            if (moment(booking_pickup_time).isSameOrAfter(moment(strtString).format('MM-DD-YYYY hh:mm')) && moment(booking_pickup_time).isSameOrBefore(moment(stopString).format('MM-DD-YYYY hh:mm'))) {
                console.log('found');
                isBusy = true;
                break;
                // return true;
            }
            // unavailableTime.start_date = moment(start_date + " " + start_time).format('DD-MM-YYYYTHH:mm:ssZ');
            // unavailableTime.stop_date = moment(stop_date + " " + stop_time).format('DD-MM-YYYYTHH:mm:ssZ');
            // unavailableTimeArr.push(unavailableTime);
        }
        return isBusy;
    }

    // get single post
    const getPost = async (req, res) => {
        const post_type = req.params.post_type;
        const post_id = req.params.post_id;
        const parent_post_id = req.post_id; // the parent Id of all child posts (company_id)
        console.log(post_id);
        console.log(post_type);
        if (post_id != null) {
            try {
                let query = '';
                let post;
                if (post_type == 'chbs_vehicle') { // get vehicles list
                    query = `Select 'vehicle_title' as meta_key, post_title as meta_value from ccB4bP116_posts where Id = ?
                    UNION Select 'imgUrl' as meta_key, guid as meta_value from ccB4bP116_posts where Id IN 
                    (Select meta_value from ccB4bP116_postmeta where post_id = ? and meta_key = '_thumbnail_id')
                    and post_status = 'publish'
                    UNION Select meta_key,meta_value from ccB4bP116_postmeta where post_id = ? group by meta_key, meta_value`;
                    post = await sequelize.query(query, { replacements: [post_id, post_id, post_id], type: QueryTypes.SELECT });
                    console.log(post.filter(x => x.meta_key == '_thumbnail_id')[0].meta_value);
                } else if (post_type == 'chbs_booking') {
                    post = await sequelize.query('CALL GetBookings(?,?,-1,0,-1)', { replacements: [parent_post_id, post_id] });

                    if (post.length > 0) {
                        let pickup_Date = moment(post[0].full_date);
                        if (pickup_Date.diff(moment(), 'h') > 24) {
                            post[0].client_phone_number = post[0].client_phone_number.substring(0, 3) + '********';
                        }

                        // come here for new changes
                        try {
                            let flight_id_serialize = post[0].flight_id;
                            let flight_id_unserialize = unserialize(flight_id_serialize);
                            post[0].flight_id = flight_id_unserialize;
                        } catch (err) {
                            console.log(err);
                        }
                    }

                } else if (post_type == 'business-hours') {
                    query = `Select Replace(meta_value,'"','^')  as buisness_hours from ccB4bP116_postmeta
                    where meta_key like 'chbs_vehicle_business_hour' 
                    and post_id = ?`; //10477
                    post = await sequelize.query(query, { replacements: [post_id], type: QueryTypes.SELECT });
                } else if (post_type == 'exclude-dates') {
                    query = `Select Replace(meta_value,'"','^') as excluded_dates from ccB4bP116_postmeta
                    where meta_key like 'chbs_date_exclude' 
                    and post_id = ?`; //10477
                    post = await sequelize.query(query, { replacements: [post_id], type: QueryTypes.SELECT });
                }

                return res.status(200).json({ post });
            } catch (err) {
                console.trace(err);
                return res.status(500).json({ msg: 'Internal server error' });
            }
        }
        return res.status(400).json({ msg: 'Bad Request: Some Parameters missing!' });
    };
    const getPost_Old = async (req, res) => {
        const body = req.body;
        console.log(body);
        const post_id = body.post_id;
        const page_params = body.page_params;
        if (post_id != null || page_params) {
            try {
                const post = await sequelize.query('CALL GetPosts(?,?)', { replacements: [post_id, page_params] });
                return res.status(200).json({ post });
            } catch (err) {
                console.trace(err);
                return res.status(500).json({ msg: 'Internal server error' });
            }
        }
        return res.status(400).json({ msg: 'Bad Request: Some Parameters missing!' });
    };

    // get selected driver for booking
    const getDriverForBooking = async (req, res) => {
        const booking_id = req.params.booking_id;
        console.log(booking_id);
        if (booking_id != null) {
            try {
                let query = `Select meta_value as driver_id from ccB4bP116_postmeta 
                where post_id = ? and meta_key = 'chbs_driver_id'`;
                let driver = await sequelize.query(query, { replacements: [booking_id], type: QueryTypes.SELECT });
                let driver_id = -1;
                if (driver.length > 0) {
                    driver_id = driver[0].driver_id;
                }
                return res.status(200).json({ driver_id });
            } catch (err) {
                console.trace(err);
                return res.status(500).json({ msg: 'Internal server error' });
            }
        }
        return res.status(400).json({ msg: 'Bad Request: Some Parameters missing!' });
    };
    const setDriverForBooking = async (req, res) => {
        const { body } = req;
        const booking_id = body.booking_id;
        const driver_id = body.driver_id;
        try {
            let query = `Update ccB4bP116_postmeta set meta_value = ? 
            where meta_key = 'chbs_driver_id' and post_id = ?`;
            let result = await sequelize.query(query, { replacements: [driver_id, booking_id] });
            let notification_result = await sendNewBookingNotification(driver_id, booking_id, 'Hai ricevuto una prenotazione');
            console.log(notification_result);
            return res.status(200).json('success');
        } catch (err) {
            console.trace(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const saveBusinessHours = async (req, res) => {
        const { body } = req;
        try {
            let rawArr = [
                `a:7:{i:1;a:2:{s:5:"start";s:5:"strt_time";s:4:"stop";s:5:"stop_time";}`,
                `i:2;a:2:{s:5:"start";s:5:"strt_time";s:4:"stop";s:5:"stop_time";}`,
                `i:3;a:2:{s:5:"start";s:5:"strt_time";s:4:"stop";s:5:"stop_time";}`,
                `i:4;a:2:{s:5:"start";s:5:"strt_time";s:4:"stop";s:5:"stop_time";}`,
                `i:5;a:2:{s:5:"start";s:5:"strt_time";s:4:"stop";s:5:"stop_time";}`,
                `i:6;a:2:{s:5:"start";s:5:"strt_time";s:4:"stop";s:5:"stop_time";}`,
                `i:7;a:2:{s:5:"start";s:5:"strt_time";s:4:"stop";s:5:"stop_time";}}`
            ];
            let stringToSave = '';
            rawArr.forEach((str, index) => {
                stringToSave += str.replace('strt_time', body.businessHours[index].start_time).replace('stop_time', body.businessHours[index].stop_time);
            });
            let query = `Update ccB4bP116_postmeta set meta_value = ? 
            where meta_key like 'chbs_vehicle_business_hour' 
            and post_id = ?`;
            let result = await sequelize.query(query, { replacements: [stringToSave, body.vehicle_id] });
            return res.status(200).json('success');
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: err });
        }
    }

    const saveUnavailableTime = async (req, res) => {
        const { body } = req;
        console.log(body.vehicle_id);
        let unavailableTimeArr = body.unavailableTimeArr;
        console.log(unavailableTimeArr);
        if (unavailableTimeArr.length > 0) {
            try {
                let stringFormat = `i:###;a:4:{s:9:"startDate";s:10:"^start_date^";s:9:"startTime";s:5:"^start_time^";s:8:"stopDate";s:10:"^stop_date^";s:8:"stopTime";s:5:"^stop_time^";}`;
                let stringToSave = '';// 'a:$$$:{';
                let _index = 0;
                unavailableTimeArr.forEach((obj, index) => {
                    _index++;
                    obj.start_time = rectifyTime(obj.start_time);
                    obj.stop_time = rectifyTime(obj.stop_time);
                    stringToSave += stringFormat.replace('###', index)
                        .replace('^start_date^', obj.start_date)
                        .replace('^start_time^', obj.start_time)
                        .replace('^stop_date^', obj.stop_date)
                        .replace('^stop_time^', obj.stop_time)
                });
                stringToSave += '}';
                let stringAtStart = 'a:' + _index + ':{';
                let finalString = stringAtStart.concat(stringToSave);
                let query = `Update ccB4bP116_postmeta set meta_value = ? 
                where meta_key like 'chbs_date_exclude' 
                and post_id = ?`;
                let result = await sequelize.query(query, { replacements: [finalString, body.vehicle_id] });
                return res.status(200).json('success');
            } catch (err) {
                console.log(err);
                return res.status(500).json({ msg: err });
            }
        } else {
            return res.status(500).json({ msg: 'Validation Error: The time is not provided.' });
        }
    }

    const deleteUnavailableTime = async (req, res) => {
        const { body } = req;
        let unavailableTimeArr = body.unavailableTimeArr;
        try {
            let finalString = '';
            if (unavailableTimeArr.length > 0) {
                let stringToSave = '';
                let stringFormat = `i:###;a:4:{s:9:"startDate";s:10:"^start_date^";s:9:"startTime";s:5:"^start_time^";s:8:"stopDate";s:10:"^stop_date^";s:8:"stopTime";s:5:"^stop_time^";}`;
                // stringToSave = 'a:1:{';
                let _index = 0;
                unavailableTimeArr.forEach((obj, index) => {
                    _index++;
                    stringToSave += stringFormat.replace('###', index)
                        .replace('^start_date^', obj.start_date)
                        .replace('^start_time^', obj.start_time)
                        .replace('^stop_date^', obj.stop_date)
                        .replace('^stop_time^', obj.stop_time)
                });
                stringToSave += '}';
                let stringAtStart = 'a:' + _index + ':{';
                finalString = stringAtStart.concat(stringToSave);
            } else {
                finalString = 'a:0:{}';
            }
            let query = `Update ccB4bP116_postmeta set meta_value = ? 
            where meta_key like 'chbs_date_exclude' 
            and post_id = ?`;
            let result = await sequelize.query(query, { replacements: [finalString, body.vehicle_id] });
            return res.status(200).json('success');
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: err });
        }

    }

    const changeVehicleAvailabilityStatus = async (req, res) => {
        const { body } = req;
        let statusObj = body;
        console.log(statusObj);
        if ('vehicle_id' in statusObj || 'meta_value' in statusObj) {
            try {
                // first delete the entry
                let query = `Delete from ccB4bP116_postmeta 
                where post_id = ? and meta_key = 'chbs_vehicle_available';`;
                let result = await sequelize.query(query, { replacements: [statusObj.vehicle_id] });
                // after deleting entry, insert new record
                query = `Insert into ccB4bP116_postmeta (post_id, meta_key, meta_value)
                values (?, 'chbs_vehicle_available', ?)`;
                result = await sequelize.query(query, { replacements: [statusObj.vehicle_id, statusObj.meta_value] });
                return res.status(200).json('success');
            } catch (err) {
                return res.status(500).json({ msg: err });
            }

        } else {
            return res.status(200).json({ msg: 'Validation error!' });
        }
    }
    const saveDriverInfo = async (req, res) => {
        const { body } = req;
        let driverInfo = body;
        const post_id = req.post_id
        console.log(driverInfo);
        try {
            let result;
            let resultNew;
            let deleteQuery = `Delete from ccB4bP116_postmeta where post_id = ? and 
            meta_key = ?`;
            let newRecQuery = `Insert into ccB4bP116_postmeta (post_id, meta_key, meta_value)
            Values (?, ?, ?)`;
            if (driverInfo.email_address) {
                result = await sequelize.query(deleteQuery, { replacements: [post_id, 'chbs_contact_email_address'] })
                resultNew = await sequelize.query(newRecQuery, { replacements: [post_id, 'chbs_contact_email_address', driverInfo.email_address] })
            } else {
                result = await sequelize.query(deleteQuery, { replacements: [post_id, 'chbs_contact_email_address'] })
                resultNew = await sequelize.query(newRecQuery, { replacements: [post_id, 'chbs_contact_email_address', ''] })
            }
            if (driverInfo.phone_number) {
                result = await sequelize.query(deleteQuery, { replacements: [post_id, 'chbs_contact_phone_number'] })
                resultNew = await sequelize.query(newRecQuery, { replacements: [post_id, 'chbs_contact_phone_number', driverInfo.phone_number] })
            } else {
                result = await sequelize.query(deleteQuery, { replacements: [post_id, 'chbs_contact_phone_number'] })
                resultNew = await sequelize.query(newRecQuery, { replacements: [post_id, 'chbs_contact_phone_number', ''] })
            }
            if (driverInfo.license_number) {
                result = await sequelize.query(deleteQuery, { replacements: [post_id, 'chbs_driving_license'] })
                resultNew = await sequelize.query(newRecQuery, { replacements: [post_id, 'chbs_driving_license', driverInfo.license_number] })
            } else {
                result = await sequelize.query(deleteQuery, { replacements: [post_id, 'chbs_driving_license'] })
                resultNew = await sequelize.query(newRecQuery, { replacements: [post_id, 'chbs_driving_license', ''] })
            }
            return res.status(200).json('success');
        } catch (err) {
            return res.status(500).json({ msg: err });
        }
    }

    const getCarsBookingCount = async (req, res) => {
        const post_id = req.post_id; // the parent Id of all child posts (company_id)
        console.log(post_id);
        if (post_id != null) {
            try {
                let query = '';
                query = `Select Count(p.ID) as totalCount
                    from ccB4bP116_posts p
                    where p.Id 
                    in (SELECT post_id FROM 
                    ccB4bP116_postmeta Where meta_value = ?) and p.post_type = 'chbs_vehicle'
                    and p.post_status = 'publish'
                    `; //10477

                let bookingsQuery = `Select Count(p.ID) as totalCount
                from ccB4bP116_posts p 
                where ((p.Id in (																																																																																																																																																																																																																																																																																																																														
                SELECT post_id FROM
                ccB4bP116_postmeta 
                Where meta_value = ?)))
                and p.post_status = 'publish'
                and p.post_type = 'chbs_booking';`;

                carsCount = await sequelize.query(query, { replacements: [post_id, post_id], type: QueryTypes.SELECT });
                bookingsCount = await sequelize.query(bookingsQuery, { replacements: [post_id, post_id], type: QueryTypes.SELECT });

                let Counts = {};
                Counts.carsCount = carsCount[0].totalCount;
                Counts.bookingsCount = bookingsCount[0].totalCount;
                return res.status(200).json({ Counts });
            } catch (err) {
                console.trace(err);
                return res.status(500).json({ msg: 'Internal server error' });
            }
        }
        return res.status(400).json({ msg: 'Bad Request: Some Parameters missing!' });
    };
    const getBillingDetail = async (req, res) => {
        const post_id = req.post_id;
        try {
            let query = `Select p.ID
            from ccB4bP116_posts p
            left join ccB4bP116_postmeta pbs on pbs.post_id = p.ID and pbs.meta_key = 'chbs_booking_status_id'
            where (p.Id in (																																																																																																																																																																																																																																																																																																																														
            SELECT post_id FROM ccB4bP116_postmeta
            Where meta_value = ?))
            and p.post_type = 'chbs_booking'
            and p.post_status = 'publish'
            and (pbs.meta_value = 2 or pbs.meta_value = 4)`;
            let result = await sequelize.query(query, { replacements: [post_id], type: QueryTypes.SELECT });

            // bookings total price with status 2. (accepted)
            let accepted_bookings_total = 0.00;
            await asyncForEach(result, async (booking) => {
                let _result = await getBookingBillingDetail(booking.ID);
                if (_result.summary) {
                    accepted_bookings_total += parseFloat(_result.summary.pay);
                } else {
                    accepted_bookings_total += 0;
                }
            })
            console.log('and total is: ');
            console.log(accepted_bookings_total);

            // bookings total price with status 5 & 6.
            query = `Select p.ID
            from ccB4bP116_posts p
            left join ccB4bP116_postmeta pbs on pbs.post_id = p.ID and pbs.meta_key = 'chbs_booking_status_id'
            where (p.Id in (																																																																																																																																																																																																																																																																																																																														
            SELECT post_id FROM ccB4bP116_postmeta
            Where meta_value = ?))
            and p.post_type = 'chbs_booking'
            and (pbs.meta_value = 5 or pbs.meta_value = 6)`;
            result = await sequelize.query(query, { replacements: [post_id], type: QueryTypes.SELECT });
            // bookings total price with status 5 & 6.
            let paid_bookings_total = 0.00;
            await asyncForEach(result, async (booking) => {
                let _result = await getBookingBillingDetail(booking.ID);
                if (_result.summary) {
                    paid_bookings_total += parseFloat(_result.summary.pay);
                } else {
                    paid_bookings_total += 0;
                }

            })
            console.log('and paid bookings total is: ');
            console.log(paid_bookings_total);
            let bookingBilling = {};
            bookingBilling.accepted_bookings_total = accepted_bookings_total;
            bookingBilling.paid_bookings_total = paid_bookings_total;
            return res.status(200).json({ bookingBilling });
        } catch (err) {
            console.trace(err);
            return res.status(500).json({ err });
        }
    }

    const getBookingsWithBilling = async (req, res) => {
        const post_id = req.post_id;
        const page_number = req.params.page_number;
        const booking_type = req.params.booking_type;
        try {
            let result = await sequelize.query('CALL GetBookingsForBilling(?,?,?)', { replacements: [post_id, page_number, booking_type] });
            await asyncForEach(result, async (booking) => {
                let _result = await getBookingBillingDetail(booking.ID);
                if (_result.summary) {
                    booking.payment = _result.summary.pay;
                } else {
                    booking.payment = 0.00;
                }

            });
            return res.status(200).json({ result });
        } catch (err) {
            console.trace(err);
            return res.status(500).json({ msg: err });
        }
    }


    const sendNotification = async (req, res) => {
        const { body } = req;
        let notificationObject = body;
        console.log(notificationObject);
        try {
            let result = await sendNewBookingNotification(notificationObject.user_id, notificationObject.booking_id, notificationObject.message);
            return res.status(200).json({ result });
        } catch (err) {
            console.trace(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }
    getBookingBillingDetail = async (booking_id) => {
        try {
            const response = await axios.get('https://www.moveu.it/wp-json/moveu-backend/v1/getBillingInfo/' + booking_id);
            // console.log(response.data);
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    sendNewBookingNotification = async (user_id, booking_id, message) => {
        try {
            const options = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic YzljMmEzODktZDliOC00NjhiLTgyNTEtMDU3MDY1NWJjYjRm'
                }
            };
            const body = {
                "app_id": "975cc4ff-93b1-4a59-bdb7-c8ed48880c69",
                "include_external_user_ids": [user_id],
                "headings": { "en": `MoveU` },
                "contents": { "en": `${message}` },
                "data": { "task": `${booking_id}` }
            };
            const response = await axios.post('https://onesignal.com/api/v1/notifications', body, options);
            console.log(response.data);
            return response.data;
        } catch (error) {
            console.log(error.response.body);
        }
    }

    setBookingToComplete = async (req, res) => {
        const { body } = req;
        let statusObj = body;
        console.log(statusObj);
        if ('post_id' in statusObj) {
            try {
                // first delete the entry
                let query = `Delete from ccB4bP116_postmeta 
                where post_id = ? and meta_key = 'chbs_booking_status_id';`;
                let result = await sequelize.query(query, { replacements: [statusObj.post_id] });
                // after deleting entry, insert new record
                query = `Insert into ccB4bP116_postmeta (post_id, meta_key, meta_value)
                values (?, 'chbs_booking_status_id', ?)`;
                result = await sequelize.query(query, { replacements: [statusObj.post_id, 4] });
                return res.status(200).json('success');
            } catch (err) {
                return res.status(500).json({ msg: err });
            }

        } else {
            return res.status(200).json({ msg: 'Validation error!' });
        }
    }
    async function asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }
    getAllIndexes = (arr, val) => {
        let indexes = [], i = -1;
        while ((i = arr.indexOf(val, i + 1)) != -1) {
            indexes.push(i);
        }
        return indexes;
    }
    rectifyTime = (time) => {
        let temp_time_arr = time.split(':');
        if (temp_time_arr[0].length < 2) {
            temp_time_arr[0] = "0" + temp_time_arr[0];
        }
        if (temp_time_arr[1].length < 2) {
            temp_time_arr[1] = "0" + temp_time_arr[1];
        }
        let temp_time = temp_time_arr[0] + ":" + temp_time_arr[1];
        return temp_time;
    }
    return {
        getPost,
        getPosts,
        setDriverForBooking,
        getDriverForBooking,
        saveBusinessHours,
        saveUnavailableTime,
        deleteUnavailableTime,
        changeVehicleAvailabilityStatus,
        saveDriverInfo,
        getCarsBookingCount,
        getDriverList,
        getBillingDetail,
        getBookingsWithBilling,
        sendNotification,
        getBookings,
        setBookingToComplete
    };
};

module.exports = PostController;