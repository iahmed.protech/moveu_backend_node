const User = require('../models/User');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const sequelize = require('../../config/database');
const { QueryTypes } = require('sequelize');

const UserController = () => {
  const register = async (req, res) => {
    const { body } = req;

    if (body.password === body.password2) {
      try {
        const user = await User.create({
          email: body.email,
          password: body.password,
        });
        const token = authService().issue({ id: user.id });

        return res.status(200).json({ token, user });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    return res.status(400).json({ msg: 'Bad Request: Passwords don\'t match' });
  };

  const login = async (req, res) => {
    const { username, password } = req.body;
    if (username && password) {
      try {
        const query = `SELECT pm.meta_id, pm.post_id,pm.meta_key, pm.meta_value, p.post_type FROM 
        ccB4bP116_postmeta pm
        inner join ccB4bP116_posts p on p.ID	= pm.post_id and p.post_status = 'publish'
        WHERE meta_key='chbs_username'
        and meta_value = ?;`;

        let user = await sequelize.query(query, { replacements: [username], type: QueryTypes.SELECT });
        if (user.length < 1) {
          return res.status(200).json({ msg: 'Bad Request: User not found' });
        }

        const post_id = user[0].post_id;

        // getting password
        const queryPassword = `SELECT meta_value FROM 
        ccB4bP116_postmeta 
        WHERE meta_key='chbs_password'
        AND post_id=?`;

        let userPassword = await sequelize.query(queryPassword, { replacements: [post_id], type: QueryTypes.SELECT });
        let orginalPassword = userPassword[0].meta_value;
        console.log(orginalPassword);
        let convertedPassword = orginalPassword.replace('$2y$', '$2a$');
        if (userPassword.length > 0) {
          let UserPassword = userPassword[0].meta_value;
        }
        if (bcryptService().comparePassword(password, convertedPassword)) {
          const token = authService().issue({ post_id: post_id });

          return res.status(200).json({ token, user });
        }

        return res.status(200).json({ msg: 'Unauthorized' });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: err });
      }
    }

    return res.status(400).json({ msg: 'Bad Request: Email or password is wrong' });
  };

  const validate = (req, res) => {
    const { token } = req.body;

    authService().verify(token, (err) => {
      if (err) {
        return res.status(401).json({ isvalid: false, err: 'Invalid Token!' });
      }

      return res.status(200).json({ isvalid: true });
    });
  };

  const getAll = async (req, res) => {
    try {
      const users = await User.findAll();

      return res.status(200).json({ users });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };


  return {
    register,
    login,
    validate,
    getAll,
  };
};

module.exports = UserController;
